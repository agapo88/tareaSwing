package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 10/07/2017.
 */
public class Programador {
    private Lenguaje[] lstLenguajes;
    private List<Lenguaje> listadoLenguajes;
    private List<Persona> listadoProgramadores;
    private int cantidadReal;

    public Programador() {
        listadoProgramadores = new ArrayList<>();
        listadoLenguajes = new ArrayList<>();
    }

    public void adicionarProgramador(Persona p ) {
        listadoProgramadores.add( p );
    }

    public void adicionarLenguaje( Lenguaje l ) {
        listadoLenguajes.add( l );
    }

    public int getCantidadReal() {
        return cantidadReal;
    }

    public void setCantidadReal(int cantidadReal) {
        this.cantidadReal = cantidadReal;
    }

    public List<Persona> getListadoProgramadores(){
        return listadoProgramadores;
    }

    public void mostrarListadoProgramadores(){

        if( listadoProgramadores.size() > 0 ){
            for (Persona p: listadoProgramadores ) {
                System.out.println(
                        "NOMBRES: " + p.getNombres() + " APELLIDOS: " + p.getApellidos() + "\n EDAD: " + p.getEdad()
                                + " GENERO: " + ( p.getGenero() ? 'M' : 'F' )
                );
            }
        }else{
            System.out.println( "No ha ingresado ningun programador, verifique. \n" );
        }
    }
}
