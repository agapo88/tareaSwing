import clases.Lenguaje;
import clases.Persona;
import clases.Programador;
import org.omg.CORBA.PERSIST_STORE;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class principal extends JDialog {
    private JPanel contentPane;
    private JButton botonGuardar;
    private JButton buttonCancel;
    private JTextField nombres;
    private JTextField apellidos;
    private JSpinner edad;
    private JTextField nacionalidad;
    private JSpinner aniosExperiencia;
    private JTable tablaLenguajes;
    private JTextField lenguaje;
    private JCheckBox genero;
    private JComboBox comboGenero;
    private JButton botonAgregar;
    private JButton eliminarSelecciónButton;
    private DefaultTableModel modelTabla;
    private DefaultComboBoxModel modelGenero;

    public principal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(botonGuardar);

        // OBJETO PROGRAMADOR
        Programador progra = new Programador();

        // MODELO TABLA
        modelTabla = new DefaultTableModel();
        modelTabla.addColumn( "No." );
        modelTabla.addColumn( "Lenguaje" );
        modelTabla.addColumn( "Compilado" );
        modelTabla.addColumn( "Editar" );

        tablaLenguajes.setModel( modelTabla );

        modelGenero = new DefaultComboBoxModel();
        modelGenero.addElement("M");
        modelGenero.addElement("F");
        comboGenero.setModel( modelGenero );

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        botonAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // OBTENER VALORES
                String varLenguaje = lenguaje.getText();
                boolean varCompilado = genero.isSelected();
                boolean error = false;

                if( varLenguaje.length() == 0 ){
                    error = true;
                    JOptionPane.showMessageDialog( getParent(), "No ha ingresado el nombre del lenguaje" );
                }

                if( !error ){
                    Lenguaje leng = new Lenguaje();

                    leng.setLenguaje( varLenguaje );
                    if( varCompilado )
                        leng.setEsCompilado( true );

                    progra.adicionarLenguaje( leng );

                    // LIMPIAR INPUTS
                    lenguaje.setText( "" );
                    genero.setSelected( false );

                    int index = modelTabla.getRowCount() + 1;
                    modelTabla.addRow(new Object[]{ index, varLenguaje, varCompilado });
                }

            }
        });
        eliminarSelecciónButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if( tablaLenguajes.getSelectedRowCount() == 0 ) {
                    JOptionPane.showMessageDialog( getParent(), "No ha seleccionado ninguna fila de la tabla" );
                }else
                {
                    modelTabla.removeRow( tablaLenguajes.getSelectedRow() );
                }
            }
        });
        botonGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //JOptionPane.showMessageDialog( getParent(), "Hola" );
                boolean error = false;

                String nombreVar = nombres.getText();
                String apellidosVar = apellidos.getText();
                String generoVar = String.valueOf(comboGenero.getSelectedItem());
                int edadVar = (int)edad.getValue();
                String nacionalidadVar = nacionalidad.getText();
                int aniosExperienciaVar = (int)aniosExperiencia.getValue();

                //JOptionPane.showMessageDialog( getParent(), "GENERO: " + edadVar );

                if( nombreVar.length() == 0 ){
                    JOptionPane.showMessageDialog( getParent(), "No ha ingresado el nombre del Programador" );
                    error = true;
                }
                else if( apellidosVar.length() == 0 ){
                    JOptionPane.showMessageDialog( getParent(), "No ha ingresado el apellido del Programador" );
                    error = true;
                }
                else if( edadVar < 10 ){
                    JOptionPane.showMessageDialog( getParent(), "La edad del Programador debe ser mayor a 9" );
                    error = true;
                }
                else if( nacionalidadVar.length() == 0 ) {
                    JOptionPane.showMessageDialog( getParent(), "No ha ingresado la nacionalidad del programador" );
                    error = true;
                }
                else if( aniosExperienciaVar == 0 ){
                    JOptionPane.showMessageDialog( getParent(), "No ha ingresado los años de experiencia del programador" );
                    error = true;
                }else if( tablaLenguajes.getRowCount() == 0 ){
                    JOptionPane.showMessageDialog( getParent(), "No ha ingresado los lenguajes que domina el programador    " );
                    error = true;
                }

                // SI NO EXISTE ERROR
                if( !error ){
                    Persona persona1 = new Persona();
                    persona1.setNombres( nombreVar );
                    persona1.setApellidos( apellidosVar );
                    if( generoVar.equalsIgnoreCase( "M" ) )
                        persona1.setGenero( true );

                    persona1.setEdad( edadVar );
                    persona1.setNacionalidad( nacionalidadVar );
                    persona1.setExperiencia( aniosExperienciaVar );

                    // SETEAR VARIABLES
                    nombres.setText( "" );
                    apellidos.setText( "" );
                    comboGenero.setSelectedIndex( 1 );
                    edad.setValue( 0 );
                    nacionalidad.setText( "" );
                    aniosExperiencia.setValue( 0 );

                    progra.adicionarProgramador( persona1 );
                }

            }
        });
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        principal dialog = new principal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
